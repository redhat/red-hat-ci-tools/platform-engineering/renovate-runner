module.exports = {
  platform: "gitlab",
  endpoint: "https://gitlab.com/api/v4/",
  onboardingConfig: {
    "$schema": "https://docs.renovatebot.com/renovate-schema.json",
    "extends": [
      "github>platform-engineering-org/.github",
      "workarounds:supportRedHatImageVersion"
    ]
  },
  onboardingRebaseCheckbox: true,
  onboardingCommitMessage: "Add renovate.json\nRelated: RHELPLAN-17567",
  commitMessageSuffix: "\nRelated: RHELPLAN-17567",
  repositories: [
    "redhat/red-hat-ci-tools/platform-engineering/renovate-runner",
    "redhat/rhel/containers/ubi9-init",
    "redhat/rhel/containers/go-toolset"
  ],
  hostRules: [
    {
      matchHost: "github.com",
      token: process.env.GITHUB_COM_TOKEN,
    },
    {
      hostType: "docker",
      matchHost: "docker.io",
      username: process.env.DOCKER_HUB_USERNAME,
      password: process.env.DOCKER_HUB_TOKEN
    },
    {
      hostType: "docker",
      matchHost: "quay.io",
      username: process.env.QUAY_USERNAME,
      password: process.env.QUAY_PASSWORD
    }
  ],
  packageRules: [
    {
      matchDatasources: "docker",
      registryUrls: ["quay.io"],
      allowedVersions: "/^\\d\\-\\d+\\.\\d+$/",
      versioning: "loose"
    }
  ]
};
